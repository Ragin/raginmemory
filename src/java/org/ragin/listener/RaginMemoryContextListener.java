package org.ragin.listener;



import java.io.File;
import javax.servlet.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.ragin.model.Tile;

/**
 *
 * @author mmueller9
 */
public class RaginMemoryContextListener implements ServletContextListener {
static final Logger LOGGER = Logger.getLogger(RaginMemoryContextListener.class);
    /**
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();

        // Logging
        // set the logfile programmatically
        // first read the location of the log4j-config-file
        // then read a separate context attribute, holding the place of the logfile
        // then replace the placeholder within the log4j.properties
        // finally initialize loggin!
        System.setProperty("application-logfile-path",sc.getInitParameter("application-logfile-path"));
        
        String fullPath = sc.getRealPath("") + File.separator + sc.getInitParameter("log4j-config-location");
        String fullPathLog = sc.getRealPath("") + File.separator +  sc.getInitParameter("application-logfile-path");
        
        PropertyConfigurator.configure(fullPath);
        System.out.println(System.getProperty("application-logfile-path"));
        LOGGER.debug("Logging to " + System.getProperty("application-logfile-path")); 
        
         // no session here, we put the original list into the context
          //add to ServletContext
          
         //sc.setAttribute("listPoolOfTiles", listPoolOfTiles);
    }

    /**
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        
    }

}
