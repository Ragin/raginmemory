/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.model;

/**
 *
 * A single Tile
 */
public class Tile {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    String imageName; 
    boolean isHidden;
    boolean isFound; 
    int id; 
    /**
     *
     * @return
     */
    public String getImageName() {
        return imageName;
    }

    /**
     *
     * @param imageName
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     *
     * @return
     */
    public boolean isIsHidden() {
        return isHidden;
    }

    /**
     *
     * @param isHidden
     */
    public void setIsHidden(boolean isHidden) {
        this.isHidden = isHidden;
    }

    /**
     *
     * @return
     */
    public boolean isIsFound() {
        return isFound;
    }

    /**
     *
     * @param isFound
     */
    public void setIsFound(boolean isFound) {
        this.isFound = isFound;
    }


    /**
     *
     * @param imageName
     * @param isHidden
     * @param isFound
     */
    public Tile(int id, String imageName, boolean isHidden, boolean isFound) {
        this.id = id; 
        this.imageName = imageName;
        this.isHidden = isHidden;
        this.isFound = isFound;
    }

    @Override
    public String toString() {
        return "Tile{" + "imageName=" + imageName + ", isHidden=" + isHidden + ", isFound=" + isFound + ", id=" + id + '}';
    }
    
    
}
