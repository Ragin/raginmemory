/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.model;

import java.util.ArrayList;
import java.util.Collections;
import org.apache.log4j.Logger;
import org.ragin.listener.RaginMemoryContextListener;

/**
 *
 * @author mmueller9
 */
public class GameDeck {

    static final Logger LOGGER = Logger.getLogger(RaginMemoryContextListener.class);
    ArrayList<Tile> gameDeckTiles = null;
    ArrayList<Tile> poolTiles ;

    public GameDeck(ArrayList<Tile> gameDeckTiles) {
        this.gameDeckTiles = gameDeckTiles;
    }

    public GameDeck() {
        // first the tiles
        poolTiles = new ArrayList<Tile>();
        for (int i = 0; i < 10; i++) {
            // id, name (wie imagename), ishidden, isfound
            Tile aTile = new Tile(i, "i" + i + ".jpg", true, false);
            poolTiles.add(aTile);
            LOGGER.debug("--> added: " + aTile);
        }
        Collections.shuffle(poolTiles);
    }

    public void setGameDeck(int numberOfTiles) {
       gameDeckTiles =  new ArrayList<Tile> ( poolTiles.subList(1, numberOfTiles+1));
        LOGGER.debug("setting game size to " + numberOfTiles);
    }

    public String showGameDeckTiles() {
        String strRet = "gameDeckTiles{";
        for (Tile singleTile : gameDeckTiles) {
            strRet += singleTile;
        }
        strRet += "}";
        return strRet;
    }

    public String showPoolTiles() {
        String strRet = "poolTiles{";
        for (Tile singleTile : poolTiles) {
            strRet += singleTile;
        }
        strRet += "}";
        return strRet;
    }
}
