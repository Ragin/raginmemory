/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.ragin.model.GameDeck;
import org.ragin.model.Tile;

/**
 *
 * @author mmueller9
 */
public class MemoryController extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(MemoryController.class);

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int numberOfTiles = 10;
        String message = "entered MemoryController";

        HttpSession session = request.getSession(true);

        // if param gamesize is set, user wants to start new game
        if (request.getParameterMap().containsKey("tiles")) {
            String strParamTiles = (String) request.getParameter("tiles");
            LOGGER.debug("Param tiles: " + strParamTiles); 
            numberOfTiles = Integer.parseInt(strParamTiles);
            // create new deck
            GameDeck myGameDeck = new GameDeck();
            myGameDeck.setGameDeck(numberOfTiles);
            //myGameDeck.setGameDeck(gamesize);
            session.setAttribute("gameDeck", myGameDeck);
            message = "created new gamedeck: " + myGameDeck.showGameDeckTiles(); 
            LOGGER.debug(myGameDeck.showGameDeckTiles()); 
        } else // existing parameter gamesize though it has no running gamedeck
        {
            LOGGER.debug("Param tiles is not defined "); 
            if (null != session.getAttribute("gameDeck")) {
                // existing game
                GameDeck myGameDeck = (GameDeck) session.getAttribute("gameDeck");
                LOGGER.debug("read deck from session: " + myGameDeck);
                LOGGER.debug(myGameDeck.showGameDeckTiles()); 
                message = "existing gamedeck: " + myGameDeck.showGameDeckTiles(); 
            }
        };

        response.setContentType("text/html");
        response.setHeader("Cache-Control", "no-cache");
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("welcome.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Main Memory controller";
    }// </editor-fold>

}
