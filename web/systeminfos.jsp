<%-- 
    Document   : SystemInfos
    Created on : 30.03.2016, 13:22:12
    Author     : Connacht
--%>

<%@page import="java.io.File"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStream"%>

<%@page import="java.util.List"%>
<%@page import="org.apache.log4j.Logger"%>

<%@include file="WEB-INF/jspf/header.jspf" %>

<header>
    <hgroup>
        <h3><span class="label label-default">System Information <%= new java.util.Date()%></span></h3>    
    </hgroup>
</header>

<table class="table table-striped">
    <%
        Logger logger = Logger.getLogger( this.getClass().getName() );
        logger.debug("entered systeminfos.jsp");
        Map<String, String> hm = new HashMap<String, String>();
        hm.put("RealPath", getServletConfig().getServletContext().getRealPath("/"));

        hm.put("user.dir", System.getProperty("user.dir")); 
        
        
        ServletContext context=getServletContext();  
        Enumeration<String> e=context.getInitParameterNames();  
        String str="";  
        while(e.hasMoreElements()){  
            str=e.nextElement();  
            hm.put("CtxParam: " + str, context.getInitParameter(str));
            //out.print("<br> "+context.getInitParameter(str));  
        }  
        
        String fullPathConfig = context.getRealPath("") + File.separator + context.getInitParameter("log4j-config-location");
        String fullPathLog = context.getRealPath("") + File.separator +  context.getInitParameter("application-logfile-path");
        hm.put("log4j properties",fullPathConfig ); 
        hm.put("log4j logfile", fullPathLog); 
        for (Map.Entry<String, String> entry : hm.entrySet()) {
            out.print("<tr><td>" + entry.getKey() + "</td><td>" + entry.getValue() + "</td></tr>");
        }

    %>    

    <tr><td>characterEncoding</td><td>${pageContext.response.characterEncoding}</td></tr>
    <tr><td>request.remoteAddr</td><td>${pageContext.request.remoteAddr}</td></tr>
    <tr><td>request.requestURI</td><td>${pageContext.request.requestURI}</td></tr>
    <tr><td>locale.country</td><td>${pageContext.request.locale.country}</td></tr>
    <tr><td>request.locale.language</td><td>${pageContext.request.locale.language}</td></tr>
 
    <tr><td>session.id</td><td>${pageContext.session.id}</td></tr>
</table>
<%@include file="WEB-INF/jspf/footer.jspf" %>