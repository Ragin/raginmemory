

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="WEB-INF/jspf/header.jspf" %>

<header>
    <hgroup>
        <h3><span class="label label-default">Log4J Logfile at <%= new java.util.Date()%></span></h3> 
    </hgroup>
</header>
<div align="center">
    <textarea rows="24" cols="150"  style="font-size: 12px;font-family:Courier New;color:#003399;white-space:pre-wrap">
<%
            Logger logger = Logger.getLogger(this.getClass().getName());
            logger.debug("entered loginfos.jsp");

            //String txtFilePath = "C:\\Program Files\\glassfish-4.1.1\\glassfish\\domains\\domain1\\config\\logs\\raginwebapp2.log";
            ServletContext ctx = config.getServletContext(); 
            String txtFilePath = System.getProperty("user.dir") + "\\" + ctx.getInitParameter("application-logfile-path");
           
            
            BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
            //BufferedReader br = new InputStreamReader(new FileInputStream(txtFilePath));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                // prepend so the last is at the top
                sb.insert(0,line + "\n");
            }
            
            out.println(sb.toString() + "</textarea>");
            out.println( "<p> File: " + txtFilePath + "</p>");            
          
        %>
    
</div>
<%@include file="WEB-INF/jspf/footer.jspf" %>