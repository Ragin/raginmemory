
<%@page import="java.io.File"%>
<%@page import="java.util.Map"%>

<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.HashMap"%>
<%@include file="WEB-INF/jspf/header.jspf" %>

<%@page import="java.sql.Connection"%>

<header>
    <hgroup>
        <h3><span class="label label-default">Project infos from web.xml <%= new java.util.Date()%></span></h3> 
    </hgroup>
</header>
        
<p><span class="label label-success">Features</span></p>
        <table class="table table-striped">
            <tr><td>Bootstrap</td></tr>
            <tr><td>Glassfish</td></tr>
            <tr><td>Gradle</td></tr>
            <tr><td>JUnit</td></tr>
            <tr><td></td></tr>
        </table>
    <hr>
   <p><span class="label label-success">Data</span></p> 
    <table class="table table-striped">
    <%
        Logger logger = Logger.getLogger( this.getClass().getName() );
        logger.debug("entered projectinfos.jsp");
        ServletContext ctx = config.getServletContext(); 
        
        Map<String, String> hm = new HashMap<String, String>();
        hm.put("getContextPath", ctx.getContextPath());
        hm.put("getServerInfo", ctx.getServerInfo());
        
        String log4jConfigFile = ctx.getInitParameter("log4j-config-location");
        String fullPath = ctx.getRealPath("") + File.separator + log4jConfigFile;
       
        for (Map.Entry<String, String> entry : hm.entrySet()) {           
            out.print("<tr><td>" + entry.getKey() + "</td><td>" + entry.getValue() + "</td></tr>");
        }
        
    %>   
      
</table>

<%@include file="WEB-INF/jspf/footer.jspf" %>